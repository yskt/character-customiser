export class AutoAnimation {
  constructor(frameTime, spriteGroup) {
    this.frameTime = frameTime;
    this.spriteGroup = spriteGroup;
    this.timer = 0;
    this.currentFrame = 0;
  }

  play(context, dt, direction, position, size) {
    if (!isNaN(dt))
    {
      this.timer += dt;
    }
    if (this.timer >= this.frameTime) {
      this.timer = 0;
      this.currentFrame++;
    }
    if (this.currentFrame >= this.spriteGroup.column) {
      this.currentFrame = 0;
    }
    this.spriteGroup.renderFrame(context, [this.currentFrame, direction], position, size);
  }
}