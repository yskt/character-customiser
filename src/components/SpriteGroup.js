export class SpriteGroup {
    constructor(row, column, tileSize, data) {
      this.row = row;
      this.column = column;
      this.tileSize = tileSize;
      this.textures = {};
      for (const key in data) {
        this.textures[key] = new Image();
        this.textures[key].src = data[key];
      }
    }

    setImage(name, color) {
      this.textures[name].src = `./src/assets/sprites/colors/${color}/${name}.png`;
    }
    
    renderFrame(context, frame, position, size) {
      for (const key in this.textures) {
        context.drawImage(
          this.textures[key], 
          frame[0]*this.tileSize, 
          frame[1]*this.tileSize, 
          this.tileSize, 
          this.tileSize, 
          position[0], 
          position[1],
          size[0],
          size[1]
        );
      }
    }
}